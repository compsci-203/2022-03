\input{../meta.tex}

\title{Lecture 13}

\begin{document}

\maketitle

\section{AC 8.5 Partition of an Integer}

\begin{frame}
    \frametitle{Paying lunch with cash}

    You forgot your \emoji{iphone} at home so you have to pay your \emoji{hamburger} with cash.
    
    How many ways can you pay \$100 with \$1, \$2, \$5 bills?
\end{frame}

\begin{frame}
    \frametitle{Paying with \$1 bills}
    
    Let $a_{1, n}$ be the number of ways to pay \$$n$ with \$1 bills.

    The GF $A_{1}(x)$ of $(a_{1, n})_{n \ge 0}$ is
    \begin{equation*}
        1 + x + x^{2} + x^{3} + \cdots = \frac{1}{1-x}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Paying with \$2 bills}
    
    Let $a_{2, n}$ be the number of ways to pay \$$n$ with \$2 bills.

    The GF $A_{2}(x)$ of $(a_{2, n})_{n \ge 0}$ is
    \begin{equation*}
        1 + x^{2} + x^{4} + x^{6} + \cdots = \frac{1}{1-x^{2}}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Paying with \$m bills}
    
    Let $a_{m, n}$ be the number of ways to pay \$$n$ with \$$m$ bills for some
    integer $m > 0$.

    The GF $A_{m}(x)$ of $(a_{m, n})_{n \ge 0}$ is
    \begin{equation*}
        1 + x^{m} + x^{2 m} + x^{3 m} + \cdots = \frac{1}{1-x^{m}}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Paying lunch with cash}

    Let $a_{n}$ be the number of ways to pay \$$n$ with \$1, \$2, \$5 bills.

    The GF of $(a_{n})_{n \ge 0}$ is
    \begin{equation*}
        \begin{aligned}
            A(x) 
            &
            =
            A_{1}(x)
            A_{2}(x)
            A_{5}(x)
            \\
            &
            =
            (1+x+x^{2} + \cdots)
            (1+x^{2}+(x^{2})^{2} + \cdots)
            \\
            &
            \qquad
            (1+x^{5}+(x^{5})^{2} + \cdots)
            \\
            &
            =
            \frac{1}{1-x}
            \frac{1}{1-x^{2}}
            \frac{1}{1-x^{5}}
        \end{aligned}
    \end{equation*}

    So the number of ways to pay \$100 is $514$
    according to
    \href{https://www.wolframalpha.com/input/?i=SeriesCoefficient\%5B1\%2F\%28\%281-x\%29\%281-x\%5E2\%29\%281-x\%5E5\%29\%29\%2C+\%7Bx\%2C+0\%2C+100\%7D\%5D}{WolframAlpha}.

    \zany{} There is no closed form for $a_{n}$.
\end{frame}

\begin{frame}
    \frametitle{Paying lunch with more cash}

    Let $P_{n}$ be the number of ways to pay \$$n$ with \$1, \$2, \$3, \$4, \ldots bills.

    The GF of $(P_{n})_{n \ge 0}$ is
    \begin{equation*}
        \begin{aligned}
            P(x) 
            &
            =
            (1+x+x^{2} + \cdots)
            (1+x^{2}+(x^{2})^{2} + \cdots)
            \\
            &
            \qquad
            (1+x^{3}+(x^{3})^{2} + \cdots)
            (1+x^{4}+(x^{4})^{2} + \cdots)
            \cdots
            \\
            &
            =
            \frac{1}{1-x}
            \frac{1}{1-x^{2}}
            \frac{1}{1-x^{3}}
            \frac{1}{1-x^{4}}
            \cdots
            \\
            &
            =
            \prod_{m=1}^{\infty}
            \frac{1}{1-x^{m}}
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{The integer partition problem}

    For \(n \in \dsN\), $P_{n}$ is also the number of integer solutions of
    \[
        a_{1}+a_{2}+\dots a_{k} = n
    \]
    for some $k \in \dsN$ such that \(a_{1} \ge a_{2} \ge \dots a_{k} > 0\).

    \cake{} What is $P_{5}$?
\end{frame}

\begin{frame}
    \frametitle{The partitions of $8$}

    How many portions of $8$ have \emph{distinct parts},
    and how many have \emph{only odd parts}.
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.9\linewidth]{./partition.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{What if you like only odd numbers?}

    Let $o_{n}$ be the number of ways to pay \$n with \$1, \$3, \$5 \ldots bills.

    Then the GF of $(o_{n})_{n \ge 0}$ is
    \begin{equation*}
        O(x) 
        = A_{1}(x) A_{3}(x) A_{5}(x) \cdots 
        = \prod_{m=1}^{\infty} \frac{1}{1-x^{2m-1}}
    \end{equation*}

    The first few of $o_{n}$ are (by \href{https://oeis.org/A000009}{OEIS})
    \begin{equation*}
        1, 1, 1, 2, 2, 3, 4, 5, 6, 8, 10, 12, 15
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{What if you like to pay with distinct bills?}

    Let $d_{n}$ be the number of ways to pay \$n with at most one of each type of \$1, \$2, \$3 \ldots bills.

    Then the GF of $(d_{n})_{n \ge 0}$ is
    \begin{equation*}
        D(x) 
        = (1+x) (1 + x^{2}) (1+ x^{3}) \cdots
        = \prod_{m=1}^{\infty} (1+x^{m})
    \end{equation*}

    The first few of $d_{n}$ are (by \href{https://oeis.org/A000009}{OEIS})
    \begin{equation*}
        1, 1, 1, 2, 2, 3, 4, 5, 6, 8, 10, 12, 15
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Proving an identity by GF}
    
    \begin{block}{Theorem 8.16 (AC)}
    For each $n \ge 1$, 
    the number of partitions of $n$ into distinct parts 
    is equal to the number of partitions of $n$ into odd parts.
    \end{block}

    Proof by showing that $O(x) = D(x)$.

    Combinatorial proof seems to be much
    \href{https://sites.math.rutgers.edu/~zeilberg/mamarim/mamarimhtml/syl84.html}{harder}.
\end{frame}

\section{AC 8.6 Exponential Generating Function}

\begin{frame}
    \frametitle{Strings consisting of one letters}
    
    Let $a_{n}$ be the number of strings of length $n$ consisting of only one letter $1$.

    The \alert{exponential generating function} of $(a_{n})_{n \ge 0}$ is
    \begin{equation*}
        A(x) = \sum_{n = 0}^{\infty} \frac{a_{n}}{n!} x^{n} = 1 + x + \frac{x^{2}}{2!} +
        \frac{x^{3}}{3!} + \cdots = e^{x}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Strings consisting of two letters}
    
    Let $b_{n}$ be the number of strings of length $n$ consisting of either letter
    $1$ or $2$.

    The \alert{exponential generating function} of $(b_{n})_{n \ge 0}$ is
    \begin{equation*}
        B(x) = \sum_{n = 0}^{\infty} \frac{b_{n}}{n!} x^{n} = 1 + (2 x) + \frac{(2 x)^{2}}{2!} +
        \frac{(2 x)^{3}}{3!} + \cdots = e^{2 x}.
    \end{equation*}
    Note that
    \begin{equation*}
        \begin{aligned}
            A(x) A(x) = e^{x} e^{x} = e^{2 x} = B(x).
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{String with even number of $0$}
    
    Let $c_{n}$ be  the number of strings consisting only of even number of $0$.

    The \alert{exponential generating function} of $(c_{n})_{n \ge 0}$ is
    \begin{equation*}
        C(x) 
        = \sum_{n = 0}^{\infty} \frac{c_{n}}{n!} x^{n} 
        = 1 + x + \frac{x^{3}}{3!} + \cdots = \frac{e^{x} + e^{-x}}{2}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Ternary strings with even number of $0$}
    
    Let $d_{n}$ be  the number of ternary strings consisting of $1$, $2$ and even number of $0$.

    The \alert{exponential generating function} of $(d_{n})_{n \ge 0}$ is
    \begin{equation*}
        D(x) = C(x) A(x) A(x) = \frac{e^{x} + e^{-x}}{2} e^{x} e^{x}
    \end{equation*}
    So
    \begin{equation*}
        d_{n} = \frac{3^{n} + 1}{2}.
    \end{equation*}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-12.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignment problems will \emph{not} be graded but they will appear in quizzes!

            \href{https://www.rellek.net/book/app-comb.html}{Applied Combinatorics}
            \begin{itemize}
                \item[\emoji{pencil}] Section 8.8: 17, 19, 21, 23, 25, 27.
            \end{itemize}

            \hint{} For some problems, you can use WolframAlpha like 
            \href{https://www.wolframalpha.com/input/?i=SeriesCoefficient\%5Bx\%2F\%281-x\%29*\%281\%2Bx\%2Bx\%5E2\%2Bx\%5E3\%29\%281\%2F\%281-x\%5E4\%29\%29\%281\%2F\%281-x\%29\%29\%2C+\%7Bx\%2C+0\%2C+n\%7D\%5D}{this}.
        \end{column}
    \end{columns}
\end{frame}

\end{document}
