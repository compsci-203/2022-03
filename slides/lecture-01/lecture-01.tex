\input{../meta.tex}

\title{Lecture 01}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{DM 0.2  Mathematical Statements}

\subsection{Atomic and Molecular Statements}

\begin{frame}
    \frametitle{Atomic and Molecular Statements}

    A \alert{statement} is any declarative sentence which is either true or false. 

    A statement is \alert{atomic} if it cannot be divided into smaller statements,
    otherwise it is called \alert{molecular}.
\end{frame}

\begin{frame}
    \frametitle{Examples of statements}
    
    These are \alert{atomic} statements
    \begin{itemize}
        \item Telephone numbers in the USA have 10 digits.
        \item 42 is a perfect square.
        \item The \emoji{crescent-moon} is made of \emoji{cheese}.
        \item Every even number greater than 2 can be expressed as the sum of two primes.
        \item 3+7=12
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Examples of non-statements}
    
    \begin{columns}[totalwidth=\textwidth]
        \begin{column}{0.5\textwidth}
            
            These are \alert{not} statements
            \begin{itemize}
                \item Would you like some \emoji{cake}?
                \item The sum of two squares.
                \item $1+3+5+7+ \cdots +2n+1$.
                \item Go to the moon!
                \item $3+x=12$.
            \end{itemize}

        \end{column}
        \begin{column}{0.5\textwidth}

            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.5\linewidth]{cake.jpg}
                \caption*{Photo from Pexels}%
            \end{figure}
            
        \end{column}
    \end{columns}

    \cake{} Can you think of some statements?
\end{frame}

\begin{frame}
    \frametitle{Molecular statements}
    
    You can build more molecular statements out of simpler ones using \alert{logical
    connectives}.

    \begin{example}[\alert{Binary connectives}]
        \begin{itemize}
            \item Sam is a \boy{} \emph{and} Chris is a \girl{}.
            \item Sam is a \boy{} \emph{or} Chris is a \girl{}.
            \item \emph{If} Sam is a \boy{}, \emph{then} Chris is a \girl{}.
            \item Sam is a \boy{} \emph{if and only if} Chris is a \girl{}.
        \end{itemize}
    \end{example}

    \pause{}

    \begin{example}[\alert{Unnary connective}]
        \begin{itemize}
            \item Sam is \alert{not} a \boy{}. (Maybe Sam is a \emoji{dog}.)
        \end{itemize}
    \end{example}
    
    \cake{} Can you think of some \alert{molecular} statements?
\end{frame}

\begin{frame}
    \frametitle{Logical connectives}
    
    \begin{center}
    \begin{tabular}{ c  c  c }
        \toprule
    Name & Notations & Read \\
    \midrule
    Conjunction & $P \wedge Q$ & ``P and Q''\\
    Disjunction & $P \vee Q$ & ``P or Q''\\
    Implication & $P \imp Q$ & ``if P then Q''\\
    Biconditional & $P \iff Q$ & ``P if and only if Q''\\
    Negation & $\neg P$ & ``not P'' \\
    \bottomrule
    \end{tabular}
    \end{center}

    $P$ and $Q$ are called \alert{propositional variables}.
\end{frame}

\begin{frame}
    \frametitle{Truth Conditions for Connectives}
    
    \begin{center}
    \begin{tabular}{ c  l }
        \toprule
    Statement & When is true? \\
    \midrule
    $P \wedge Q$ & $P=\txtcheck{}$ and $Q=\txtcheck{}$\\
    $P \vee Q$ & $P=\txtcheck{}$ or $Q=\txtcheck{}$\\
    $P \imp Q$ &  $P=\txtcross{}$ or $Q=\txtcheck{}$ or both\\
    $P \iff Q$ & $P=Q=\txtcheck{}$, or  $P=Q=\txtcross{}$\\
    $\neg P$ & $P=\txtcross{}$  \\
    \bottomrule
    \end{tabular}
    \end{center}
\end{frame}

\subsection{Implications}

\begin{frame}
    \frametitle{Implications}

    An \alert{implication} is a molecular statement of the form $P \imp Q$.

    We say that
    \begin{itemize}
        \item $P$ is the \alert{hypothesis} (not to confused with \emph{hypnosis}
            \sweat{}).
        \item $Q$ is the \alert{conclusion}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Mathematics statements}

    Most statements in mathematics are implications.

    \begin{columns}[totalwidth=\textwidth]
        \begin{column}{0.6\textwidth}

            \begin{block}{Pythagorean Theorem}
                If $a$ and $b$ are the legs of a right triangle with hypotenuse $c$, 
                then
                \begin{equation*}
                    a^2 + b^2 = c^2.
                \end{equation*}
            \end{block}

        \end{column}
        \begin{column}{0.4\textwidth}

            \begin{figure}
                \centering
                \includegraphics[width=0.7\textwidth]{Pythagorean.png}
                \caption*{From
                \href{https://commons.wikimedia.org/w/index.php?curid=640875}{Wikipedia}}
            \end{figure}

        \end{column}
    \end{columns}

    \vspace{1em}

    \cake{} Why isn't $a^2+b^2=c^2$ itself a \emph{statement}?
\end{frame}

\begin{frame}[t]
    \frametitle{True or false?}
    \vspace{1em}

    \begin{columns}[t, totalwidth=\textwidth]
        \begin{column}{0.5\textwidth}
            When is $P \imp Q = \txtcheck{}$?
            \begin{itemize}
                \item $P= Q = \txtcheck{}$, or
                \item $P=\txtcross{}$.
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            When is $P \imp Q =\txtcross{}$?
            \begin{itemize}
                \item $P= \txtcheck{}$ and $Q = \txtcross{}$.
            \end{itemize}
        \end{column}
    \end{columns}

    \pause{}

    \begin{exampleblock}{When am I lying?}
    Assume that I told Bob \emph{If you get a 90 on the final, then you will pass the
    class.}
    In which case can Bob call me a liar \sweat{}?
    \end{exampleblock}

    \pause{}
    \bomb{} ``P implies Q'' does \alert{not} mean $P$ causes or is the reason of $Q$.
\end{frame}

\begin{frame}
    \frametitle{\exercise{}}

    Which of the following statements are true?
    \begin{enumerate}
        \item If $1=1$, then most horses have 4 legs.
        \item If $0=1$, then $1=1$.
        \item If $8$ is a prime number, then the 7624th digit of $\pi$ is an $8$.
        \item If the 26th digit of $\pi$ is an $3$, then $2+2=5$.
    \end{enumerate}
\end{frame}

\begin{frame}[t]
    \frametitle{Direct proof of implications}
    
    To prove an implication $P \imp Q$, it is enough to \emph{assume} $P =
    \txtcheck{}$, and from it, deduce $Q = \txtcheck{}$.
    
    \begin{exampleblock}{Example}
        Prove that if two numbers $a$ and $b$ are even, then their sum $a + b$ is even.
    \end{exampleblock}
\end{frame}

\subsection{Converses and Contrapositives}

\begin{frame}
    \frametitle{Converses}

    The \alert{converse} of $P \imp Q$ is $Q \imp P$.

    The converse \alert{$\ne$} the original implication.

    \begin{exampleblock}{Example}
        Implication: If an integer is greater than 2 is prime, then that number is odd.

        Converse: If an integer is odd, then it is a prime number greater than 2.

        \cake{} Are these statement true?
    \end{exampleblock}
\end{frame}

\begin{frame}[t]
    \frametitle{Contrapositives}

    The contrapositive of $P \imp Q$ is the statement $\neg Q \imp \neg P$. 

    An implication \alert{$=$} its contrapositive.

    \begin{example}[Proof by contrapositives]
        If you draw at least nine playing cards from a deck,
        then you will have at least three cards all of the same suit
        (\emoji{spade-suit} \emoji{heart-suit} \emoji{diamond-suit}
        \emoji{club-suit}). 
    \end{example}

    \emoji{video-game} You can try this \href{https://www.random.org/playing-cards/}{here}.

    \cake{} Is the \alert{converse} true?
\end{frame}

\begin{frame}
    \frametitle{Susan's grade}
    
    Suppose that \emph{if Susan gets a 93\% on her final, then she will get an A in
    the class}.

    What can you conclude in the following cases:

    \begin{itemize}
        \item Susan gets a 93\% on her final.
        \item Susan gets an A in the class.
        \item Susan does not get a 93\% on her final.
        \item Susan does not get an A in the class.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{\exercise{}}

    Consider \emph{If you will give me a \emoji{panda}, then I will give you \emoji{unicorn}.}

    Are the statements below its \emph{converse}, \emph{contrapositive} or
    \emph{neither}?

    \begin{enumerate}
        \small
        \item If you will give me a \emoji{panda}, then I will not give you \emoji{unicorn}.
        \item If I will not give you \emoji{unicorn}, then you will not give me a \emoji{panda}.
        \item If I will give you \emoji{unicorn}, then you will give me a \emoji{panda}.
        \item If you will not give me a \emoji{panda}, then I will not give you \emoji{unicorn}.
        \item You will give me a \emoji{panda} and I will not give you \emoji{unicorn}.
    \end{enumerate}
\end{frame}

\begin{frame}[t]
    \frametitle{If and only if/Biconditional}
    
    When $P \imp Q$ and $Q \imp P$ are both true, we write 
    $$P \iff Q$$
    which reads
    \begin{equation*}
        P \text{ \alert{if and only if} } Q.
    \end{equation*}

    In other words, 
    \begin{equation*}
        P \iff Q = (P \imp Q) \wedge (Q \imp P).
    \end{equation*}

    \begin{example}
        An integer $n$ is even if and only if $n^{2}$ is even.
    \end{example}
\end{frame}

\begin{frame}
    \frametitle{Which is \alert{if} which is \alert{only if}?}

    Let 
    \begin{enumerate}
        \item $P$ be \emph{I \emoji{microphone}},
        \item $Q$ be \emph{I'm in the \emoji{shower}}.
    \end{enumerate}
    Then
    \begin{center}
    \begin{tabular}{ l  l}
        \toprule
    Math & English \\
    \midrule
        $P \iff Q$ & I \emoji{microphone} if and only if I'm in the \emoji{shower}.\\
        $Q \imp P$ & I \emoji{microphone} if I'm in the \emoji{shower}.\\
        $Q \imp P$ & If I'm in the \emoji{shower}, then I \emoji{microphone}.\\
        $P \imp Q$ & \question{} \\
        $P \imp Q$ & \question{} \\
    \bottomrule
    \end{tabular}
    \end{center}
\end{frame}

%\begin{frame}
%    \frametitle{Which is \alert{if} which is \alert{only if}?}
%
%    🤔️ Let 
%    \begin{enumerate}
%        \item $P$ be \emph{I 📞️ my mum}
%        \item and $Q$ be \emph{I need 💰️}.
%    \end{enumerate}
%    Write the following in term of $P$ and $Q$.
%    \begin{enumerate}
%        \item I 📞️ my mum if I need 💰️.
%        \item If I 📞️ my mum, I need 💰️.
%        \item If I need 💰️, then I 📞️ my mum.
%        \item I 📞️ my mum only if I need 💰️.
%        \item I 📞️ my mum if and only if I need 💰️.
%    \end{enumerate}
%    
%    \practice{}
%\end{frame}

\begin{frame}
    \frametitle{Necessity and sufficiency}
    
        ``$P$ is \alert{necessary} for $Q$'' means $Q \imp P$.

        ``$P$ is \alert{sufficient} for $Q$'' means $P \imp Q$.

        If $P$ is \alert{necessary and sufficient} for $Q$, then $P \iff Q$.

    \pause{}

    \begin{exampleblock}{Necessity}
        For Sam to be a \emoji{rabbit}, it is \alert{necessary} for Sam to be an animal.

        For Sam to be a \emoji{rabbit}, it is \alert{not sufficient} for Sam to be an
            animal. (Sam can be a \emoji{tiger}.)
    \end{exampleblock}

    \pause{}

    \begin{exerciseblock}{Sufficiency}
        For Chris to be a plant, it is \alert{sufficient} for Chris to be a \emoji{sunflower}.

        For Chris to be a plant, it is \alert{not necessary} for Chris to
            be a \emoji{sunflower}. (Chris can be a \emoji{cactus}.)
    \end{exerciseblock}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Consider the statement 
    \begin{itemize}
        \item If I dream, then I am \emoji{sleeping}.
    \end{itemize}
    Which of the following is equivalent to it?

    \begin{enumerate}
        \item To be \emoji{sleeping}, it is sufficient to dream.
        \item I am \emoji{sleeping} if I dream.
        \item I dream only if I am \emoji{sleeping}.
        \item In order to dream, I must be \emoji{sleeping}.
        \item I am not dreaming unless I am \emoji{sleeping}.
        \item To dream, it is necessary that I am \emoji{sleeping}.
    \end{enumerate}
    
    \bomb{} There are many ways to state an implication in English.
\end{frame}

%\begin{frame}
%    \frametitle{Exercise --- Rephrase an implication}
%
%    Consider the statement ``if I dream, then I am \emoji{sleeping}''.
%    Which of the following is equivalent to its \emph{converse}?
%
%    \begin{enumerate}
%        \item To be \emoji{sleeping}, it is sufficient to dream.
%        \item I am \emoji{sleeping} if I dream.
%        \item I dream only if I am \emoji{sleeping}.
%        \item In order to dream, I must be \emoji{sleeping}.
%        \item I am not dreaming unless I am \emoji{sleeping}.
%        \item To dream, it is necessary that I am \emoji{sleeping}.
%    \end{enumerate}
%
%    \practice{}
%\end{frame}

\appendix{}

\begin{frame}
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-01.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignment problems will \emph{not} be graded but they will appear in quizzes!

            \href{http://discrete.openmathbooks.org/dmoi3}{Discrete Mathematics}, 
            \begin{itemize}
                \item[\emoji{pencil}] 
                    Section 0.2: 1, 3, 4, 5, 6, 7, 9, 10.
            \end{itemize}
        \end{column}
    \end{columns}

\end{frame}

\end{document}
