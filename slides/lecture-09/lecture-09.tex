\input{../meta.tex}

\title{Lecture 09}

\begin{document}

\maketitle

\section{AC 7 Inclusion-Exclusion}

\subsection{7.1 Introduction}

\begin{frame}
    \frametitle{The number of CS students}
    
    Let $X$ be the set of $\underline{\hspace{1cm}}$ students in a discrete math course. 

    Suppose there are $\underline{\hspace{1cm}}$ computer/data science majors 
    and $\underline{\hspace{1cm}}$ \emoji{boy} students. 

    Also, we know there are $\underline{\hspace{1cm}}$ \emoji{boy} 
    students majoring in computer/data science. 

    \think{} How many students in the class are \emoji{girl} students 
    \emph{not} majoring in computer science?
\end{frame}

\begin{frame}
    \frametitle{A Venn Diagram}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{cs-student.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Amanda's money again}
    
    What is the number integer solutions for
    \begin{equation*}
        x_1 + x_2 + x_3 + x_4 = 97
    \end{equation*}
    with $x_{1}, x_{2}, x_{3}, x_{4} \ge 0$, $x_{3} \le 7$ and $x_{4} \le 8$.
\end{frame}

\subsection{7.2 The Inclusion-Exclusion Formula}

\begin{frame}
    \frametitle{Notations}
    
    Let $X$ be a set and let $\scP = \{P_1 , P_2 , \ldots, P_m \}$ be a family of properties. 

    For a subset $S \subseteq [m]$, let $N(S)$ denote the number of elements of $X$
    which satisfy property $P_i$ for all $i \in S$. 

    \bomb{} If $S = \emptyset$, then $N(S) = |X|$.
\end{frame}

\begin{frame}
    \frametitle{The CS student example}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{cs-student.png}
    \end{figure}

    Let $X$ be the set of students.
    Let $P_{1}$ be \emph{male} and $P_{2}$ be \emph{CS Major}.

    \cake{} Then what is the number
    \begin{equation*}
        N(\emptyset) - N(\{1\}) - N(\{2\}) + N(\{1, 2\}).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Amanda's example}
    
    Let $X$ be the set of integer solutions for
    \begin{equation*}
        x_1 + x_2 + x_3 + x_4 = 100
    \end{equation*}
    with $x_{1}, x_{2}, x_{3}, x_{4} \ge 0$.
    Let $P_{1}$ be \emph{$x_{3} > 7$} and $P_{2}$ be \emph{$x_{4} > 8$}.

    \cake{} Then what is the number
    \begin{equation*}
        N(\emptyset) - N(\{1\}) - N(\{2\}) + N(\{1, 2\}).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Three properties}
    
    Let $P_{1}, P_{2}, P_{3}$ be having a \emoji{dog-face}, a \emoji{cat-face}, a \emoji{pig-face} respectively.
    What is the number of students having none of these pets?

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{venn-3.png}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Four properties}
    
    Let $P_{1}, P_{2}, P_{3}, P_{4}$ be having a \emoji{dog-face}, a
    \emoji{cat-face}, a \emoji{pig-face}, a \emoji{horse-face} respectively.
    What is the number of students having none of these pets?
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{venn-4.png}
            \end{figure}
            
        \end{column}
        \begin{column}{0.5\textwidth}
            
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Principle of inclusion-exclusion}

    \begin{block}{Theorem 7.7}
        The number of elements of $X$ which satisfy \emph{none} of the properties in $\scP =
        \{P_{1}, P_{2}, \ldots, P_{m}\}$ is given by
        \begin{equation*}
            \sum_{Y \subseteq [m]} (−1)^{|Y|} N(Y),  \tag{7.2.1}
        \end{equation*}
        where $[m] = \{1, \dots, m\}$.
    \end{block}

    \emoji{smile} Proof by induction!

    \cake{} What does the theorem say when $m =1$?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    How many positive integers less than or equal to 
    $100$ are divisible by \emph{none} of $2$, $3$, and $5$?
\end{frame}

\subsection{7.3 Enumerating Surjections}

\begin{frame}
    \frametitle{Surjective (onto) functions}

    When a function's range contains the codomain, we
    say that is a \alert{surjective/onto function} or a \alert{surjection}.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{surjective.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Lottery tickets}
    
    We have 15 \emph{distinct} \emoji{fish} and we want to give them
    to our five \emoji{cat-face}.

    We want each \emoji{cat-face} gets at least one \emoji{fish}.  

    In how many ways can we do this?

    \pause{}

    \hint{} This is to ask for the number of surjections from $[15]$ to $[5]$.
    
    \bomb{} The problem is different to distributing 15 \emph{identical} \emoji{coin}.
\end{frame}

\begin{frame}
    \frametitle{Notations}
    
    Let $S(n, m)$ be the number of surjections from $[n]$ to $[m]$.

    Let $X$ be the set of all functions from $[n]$ to $[m]$.

    $f \in X$ satisfies $P_{i}$ if $i$ is \emph{not} in the range of $f$.
    ($\temoji{cat-face}_i$ does not have a \emoji{fish}.)

    \pause{}

    \cake{} Why is $S(n, m) = 0$ for all $n < m$?

    \cake{} For $Y = \{1, 3\} \subseteq [m]$, what does $N(Y)$ mean?
\end{frame}

%\begin{frame}
%    \frametitle{Do you understand the notations?}
%    
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=\linewidth]{surjections.png}
%    \end{figure}
%\end{frame}

\begin{frame}[t]
    \frametitle{Lemma 7.8}

    For $Y \subseteq [m]$ with $\abs{Y} = k$, we have
    \begin{equation*}
        \abs{N(Y)} = (m-k)^{n}.
    \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{The number of surjections}

    \begin{block}{Theorem 7.9}
        The number $S(n, m)$ of surjections from $[n]$ to $[m]$ is given by:
        \begin{equation*}
            S(n,m)
            =
            \sum_{k=0}^{m} (-1)^{k} \binom{m}{k}(m-k)^{n}
            .
        \end{equation*}
    \end{block}
\end{frame}

%\begin{frame}
%    \frametitle{Can you answer lottery question?}
%    
%    The first four terms of $(S(15,i))_{i \ge 1}$ are
%    \begin{equation*}
%        1, 32766, 14250606, 1016542800
%    \end{equation*}
%    Can you find $S(15, 5)$?
%    Maybe 
%    \href{https://oeis.org/search?q=1\%2C+32766\%2C+14250606\%2C+1016542800&sort=&language=english&go=Search}{OEIS}
%    can help?
%
%    What about $\left(\frac{S(15,i)}{i!}\right)_{i \ge 1}$?
%    \begin{equation*}
%        1, 16383, 2375101, 42355950
%    \end{equation*}
%    Check \href{https://oeis.org/search?q=1\%2C+16383\%2C+2375101\%2C+42355950&language=english&go=Search}{OEIS} again.
%\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-09.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignment problems will \emph{not} be graded but they will appear in quizzes!

            \href{https://www.rellek.net/book/app-comb.html}{Applied Combinatorics}
            \begin{itemize}
                \item[\emoji{pencil}] Section 7.7: 1, 3, 5, 7, 9, 11, 13, 15, 17.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
