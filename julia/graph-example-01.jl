import Cairo, Fontconfig
using Graphs, GraphPlot, Compose

## Create a simple graph
g = SimpleGraph(5);
edgelist = [(1,2), (3,4), (1,4)]
for e in edgelist
    add_edge!(g, e...)
end

## Set node labels
nodelabel = 'a':'e'

## Save the graph
for i in 1:6
    layout=(args...)->spring_layout(args...; C=15+2*i)
    plt = gplot(g, nodelabel=nodelabel, layout=layout, NODESIZE=0.2, NODELABELSIZE=12)
    draw(PNG("/tmp/graph-01-$i.png", 16cm, 16cm, dpi=300), plt)
end