import Cairo, Fontconfig
using Graphs, GraphPlot, Compose

## Create a simple graph
g = CompleteGraph(5)

## Set node labels
nodelabel = 'a':'e'

## Layout list
layoutlist = [spring_layout, random_layout, random_layout, circular_layout, spectral_layout, random_layout]

## Save the graph
for i in 1:6
    plt = gplot(g, nodelabel=nodelabel, layout=layoutlist[i], NODESIZE=0.2, NODELABELSIZE=12)
    draw(PNG("/tmp/graph-k5-$i.png", 16cm, 16cm, dpi=300), plt)
end