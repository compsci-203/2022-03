\input{../meta.tex}

\title{Lecture 22}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{AC 5.4 Graph Colouring}

\subsection*{5.4.3 Can We Determine Chromatic Number?}

\begin{frame}
    \frametitle{Computational Complexity}
    
    \emoji{worried} Graph colouring is computationally hard. 

    It is \href{https://en.wikipedia.org/wiki/NP-complete}{NP-complete} 
    to decide if $\chi(G) = k$ for a given $k$ 
    except for the cases $k \in \{0,1,2\}$. 

    \begin{block}{NP-complete Problems}
        It is a problem for which the correctness of each solution can be verified
        \emph{quickly} and a brute-force search algorithm can
        find a solution by trying all possible solutions.

        The problem can be used to solve every other problem of this type. 
        In this sense, NP-complete problems are the \emph{hardest} of all such problems.
    \end{block}

    \cake{} Why it easy if $k \in \{0, 1, 2\}$?
\end{frame}

\begin{frame}[c]
    \frametitle{First fit algorithm}

    A naive algorithm to find a proper colouring --
    \begin{itemize}
        \item Fix an ordering of the vertex set $\{v_1 , v_2 , . . . v_n\}$.
        \item Let $\phi(v_{1}) = 1$.
        \item Given of $\phi(v_1), \phi(v_2), \ldots, \phi(v_i)$, let $\phi(v_i+1)$
            be the smallest integer (colour) which we can give $v_{i+1}$.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Colouring a graph greedily \emoji{heart-eyes}}

    Can you try colouring the vertices one-by-one \emph{properly} with as few colours as possible?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{./colouring-2.png}
        \caption*{The same graph with different vertex ordering}%
        \label{fig:./colouring-2}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Brook's Theorem}

    \begin{block}{Theorem 4.4.5 (DM)}
        Let $\Delta(G)$ be the maximum degree in $G$.

        Any graph $G$ satisfies $χ(G) \le ∆(G)$, 
        unless $G$ is a complete graph or an odd cycle, 
        in which case $χ(G) = ∆(G) + 1$.
    \end{block}

    \pause{}

    \think{} Why does first-fit implies that $\chi(G) \le \Delta(G) + 1$ where $\Delta(G)$
    is the maximum degree in $G$?
\end{frame}

\begin{frame}[c]
    \frametitle{Interval Graph}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{./interval.png}
    \end{figure}

    Given a list of intervals $(S_{v})_{v \in V}$, the \emph{interval graph} is the
    graph with vertex set $V$ and edge set
    \begin{equation*}
        E = \{ uv : S_{u} \cap S_{v} \ne \emptyset\}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    \cake{} Can You Draw the Interval Graph?

    \bigskip
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{Interval_graph.svg.png}
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{When does chromatic number equal clique number?}

    \begin{block}{Theorem 5.28 (AC)}
        If $G = (V, E)$ is an interval graph, then $\chi(G) = \omega(G)$.
    \end{block}

    Proof sketch --
    \begin{itemize}
        \item Order the vertices according to their left-endpoint.
        \item Run the first-fit algorithm.
        \item Assume that we colour $v_{i}$ with the last colour $\chi(G)$. What does this
            tell us about $\omega(G)$?
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{An example to help you think}

    Try colouring this graph with first-hit.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.45\linewidth]{./interval-2.pdf}
        \includegraphics[width=0.45\linewidth]{./interval-1.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Perfect Graphs}
    
    A graph $G$ is said to be \alert{perfect} if $\chi(H) = \omega(H)$ for every
    \emph{induced} subgraph $H$. 

    The following interval graph is perfect?
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.35\linewidth]{./interval-2.pdf}
        \includegraphics[width=0.35\linewidth]{./interval-1.pdf}
    \end{figure}

    \bonus{} Is all following interval graphs \emph{perfect}? Why?
\end{frame}

\begin{frame}
    \frametitle{When is $K_{n}$ perfect?}

    \bonus{} Can you give \alert{two} different proofs that the following graph is perfect?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.3\linewidth]{./interval-5.pdf}
        \caption*{$K_{10}$}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Strong Perfect Graph Theorem}
    
    A graph is perfect if and only if it has neither odd holes 
    (odd-length induced cycles of length at least $5$) nor odd anti-holes (complements of odd holes).

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\textwidth]{c5.png}
        \includegraphics[width=0.4\textwidth]{c5-complement.png}
        \caption*{$C_{5}$ and its complement}
    \end{figure}

    \cake{} Why does the theorem imply that $K_{n}$ is perfect?
\end{frame}

\begin{frame}[c]
    \frametitle{Maria Chudnovsky}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}

            The theorem was proved by Maria Chudnovsky, Neil Robertson, Paul Seymour, and
            Robin Thomas (2006).

            \bigskip

            One of the authors, Maria Chudnovsky won MacArthur Fellow of 2012, which comes
            with a grant \$500,000, no strings attached.

        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\linewidth]{./maria.jpg}
                \caption*{Maria Chudnovsky}%
            \end{figure}

        \end{column}
    \end{columns}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-22.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignment problems will \emph{not} be graded but they will appear in quizzes!

            \href{https://www.rellek.net/book/app-comb.html}{Applied Combinatorics}
            \begin{itemize}
                \item[\emoji{pencil}] Section 5.9: 19, 21, 22, 23.
            \end{itemize}

            \href{http://discrete.openmathbooks.org/dmoi3}{Discrete Mathematics}, 
            \begin{itemize}
                \item[\emoji{pencil}] 
                    Section 4.4: 1, 2, 3, 5, 7, 9, 10, 12.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
