td = Dict{Int, Int}()

function tile(n)
    get!(td, n) do
        if n <=1
            2
        elseif n == 2
            5
        else
            2 * tile(n-3) + tile(n-2) + 2 * tile(n-1)
        end
    end
end

ts = [tile(n) for n in 1:6]