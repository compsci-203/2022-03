import Cairo, Fontconfig
using Graphs, GraphPlot, Compose, Random

## Save the graph
Random.seed!(1235)
for i in 1:3
    n = 10
    p = 0.4
    g = Graphs.SimpleGraphs.erdos_renyi(n, p)
    cliques = maximal_cliques(g)
    cliquenum = maximum(length.(cliques))
    println("Clique number is $cliquenum")
    nodelabel = 1:n
    plt = gplot(g, nodelabel=nodelabel, NODESIZE=0.1, NODELABELSIZE=9)
    draw(PNG("/tmp/clique-number-01-$i.png", 16cm, 16cm, dpi=300), plt)
end