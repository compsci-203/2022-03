\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 27}

\begin{document}

\maketitle

\section{AC 11 Applying Probability to Combinatorics}

\subsection{AC 11.1 A First Taste of Ramsey Theory}

\begin{frame}
    \frametitle{A six-student problem}
    
    Six students are taking COMPSCI 203.

    \think{}
    Is it true that there are always three students who know
    each other from previous courses, 
    or three people who does not know each other from before?

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{./ramsey-party.pdf}
        \caption{Some possible cases}%
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{A six people lemma}
    
    \begin{block}{Lemma 11.1 (AC)}
        Let $G$ be any graph with six vertices. 
        Then either $G$ contains a $K_{3}$ or an $I_{3}$ as subgraphs.
    \end{block}

    \pause{}

    Note that five vertices does not work. $C_{5}$ contains neither. 

    \cake{} What about $7, 8, 9, \ldots$ vertices?
\end{frame}


\begin{frame}
    \frametitle{Ramsey's theory for graphs}

    \begin{block}{Theorem 11.2 (AC)}
        If $m, n \in \{1,2,3\dots\}$,
        then there exists a least positive integer $R(m, n)$ 
        so that if $G$ is a graph and $G$ has at least $R(m, n)$ vertices, 
        then either $G$ contains $K_{m}$ or $I_{n}$.
    \end{block}

    Note that $R(3,3) = 6$.

    \pause{}

    \cake{} What is $R(1, 1)$? 
    What about $R(2, 1), R(2, 1), R(2,1), \ldots$?

    \pause{}

    \bonus{} What is $R(2, 2)$? 
    What about $R(3, 2), R(4, 2), R(5,2), \ldots$?
\end{frame}

\begin{frame}[t]
    \frametitle{Proof of Theorem 11.2}

    We use induction on $t = m+n$ to show that $R(m,n) \le \binom{m+n-2}{m-1}$.

    \only<1>{Induction bases: Trivial to verify for $t \le 5$.}

    \only<2>{
        Induction step: 
        Assume true for any $t \le m+n -1$.

        Let $x$ be any vertex in $G$ with $\binom{m+n-2}{m-1}$ vertices.
        Let $S_{1}$ be the vertices adjacent to $x$ and $S_{2}$ be the vertices which are not. 
    }
\end{frame}

\subsection{11.2 Small Ramsey Numbers}

\begin{frame}[t]
    \frametitle{$R(4,3)$}

    Note that $R(4,3) \le \binom{4+3-2}{3-1} = 10$. In fact, $R(4,3)=9$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{./ramsey-large-party.pdf}
        \caption{$R(4,3)=9$}%
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{Lower bound for $R(4,3)$}
    
    Why do the following graphs prove that $R(4,3) > 8$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{./ramsey-cyc.pdf}
        \caption{Proof that $R(4,3) > 8$}%
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Small Ramsey Numbers}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{./ramsey.png}
        \caption{Small Ramsey numbers}%
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{What Erdős said about Ramsey numbers}

    \begin{columns}[c]
        \begin{column}{0.6\textwidth}
            Say \emoji{alien} from far outer space want to know $R(5,5)$,
            or they'll wipe out the whole human race.

            If we join all our forces, perhaps we'll contrive
            to tell them the value of $R(5,5)$.

            But we'll certainly be in a hell of a fix
            if they ask for the value of $R(6,6)$.

            \hfill --- Erdős
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{./Erdos.jpg}
                \caption{From
                    \href{https://en.wikipedia.org/wiki/Paul_Erd\%C5\%91s}{Wikipedia}}%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\subsection{11.3 Estimate Ramsey Number}

\begin{frame}[c]
    \frametitle{Stirling's approximation}
    
    To approximate $n!$, we can use \href{https://mathworld.wolfram.com/StirlingsApproximation.html}{Stirling's approximate}
    \begin{equation*}
        n! = \sqrt{2 \pi n } \left(\frac{n}{e}\right)^{n} 
        \left(1+\frac{1}{12
        n}+\frac{1}{288 n^2}-\frac{139}{51840
    n^3}+O\left(\left(\frac{1}{n}\right)^4\right)\right),
    \end{equation*}
    or the simpler version
    \begin{equation*}
        n! = \sqrt{2 \pi n } \left(\frac{n}{e}\right)^{n} 
        \left(1+O\left(\frac{1}{n}\right)\right).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{A numeric verification}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{./ramsey-stirling.pdf}
        \caption{$n!/\sqrt{2 \pi n } \left(\frac{n}{e}\right)^{n}$}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{How to Estimate Ramsey Number?}

    Given that
    \begin{equation*}
        n! = \sqrt{2 \pi n } \left(\frac{n}{e}\right)^{n} 
        \left(1+O\left(\frac{1}{n}\right)\right).
    \end{equation*}
    we have
    \begin{equation*}
        R(n,n) \le \binom{2 n - 2}{n-1} \approx \frac{2^{2n}}{4 \sqrt{\pi n}}.
    \end{equation*}
\end{frame}

\end{document}
