\input{../meta.tex}

\title{Lecture 15}

\begin{document}

\maketitle

\section{AC 9 Recurrence Equations}

\subsection{9.4.2 Solving Advancement Operator Equations -- Nonhomogeneous Case}

\begin{frame}
    \frametitle{Nonhomogeneous equations}
    Consider the homogeneous equation
    \[
        p(A) f = (A+2)(A-6) f = 0.
    \]
    \cake{} The solutions are of the form
    \[
        f_{1}(n) = \txtq{}
    \]

    \think{} What is the general solution for
    \[
        p(A) f = (A+2)(A-6) f = 3^{n}
    \]
\end{frame}

\begin{frame}{Finding a particular solution}
    Can you find any \alert{one} solution for
    \[
        p(A) f = (A+2)(A-6) f = 3^{n}
    \]
    \sweat{} No general method is known. 

    \hint{} Try something like the RHS, like, \(d 3^{n}\). 

    \pause{}

    This gives one \alert{particular} solution
    \[
        f_{2}(n)= \txtq{}
    \]

    So the general solution is
    \[
        f(n)=f_{1}(n) + f_{2}(n).
    \]

    \hint{} \emph{All} solutions are of this form. (Ask your linear algebra
    professor!)
\end{frame}

\begin{frame}
    \frametitle{A recipe \emoji{ramen} for nonhomogeneous equations}
    We want to solve problems like 
    \[
        p(A) f = g.
    \]
    First we find the \emph{general} solution \(f_{1}\) for 
    \[
        p(A) f = 0
    \]
    Second we find (any) \emph{particular} solution (by guessing) \(f_{2}\) for 
    \[
        p(A) f = g.
    \]
    Then the general solution is
    \[
        f(n)=f_{1}(n)+f_{2}(n)
    \]
\end{frame}

\begin{frame}
    \frametitle{Example 9.15}
    
    Find the solutions to the equation
    \begin{equation*}
        (A + 2)(A − 6) f = 6^n,
    \end{equation*}
    if $f(0) = 1$ and $f(1) = 5$.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    \bonus{} Find the solutions to the equation
    \begin{equation*}
        (A^2 - 3 A - 10)f = 3^n.
    \end{equation*}
\end{frame}

%\begin{frame}[t]
%    \frametitle{Lines and kingdoms --- closed from}
%    Find a particular solution of
%    \begin{equation}
%        \label{eq:nonhomogeneous:1}
%        r(n+1)-r(n)=n+1 
%        \qquad \longleftrightarrow \qquad
%        (A-1)r = n + 1
%    \end{equation}
%    in the form of $d_{1} n^{2} + d_{2} n$.
%
%    \cake{} What are \alert{all} the solutions \cref{eq:nonhomogeneous:1}?
%
%    %Given that $r(0) = 1$ and $r(1)=2$, what is the solution of
%    %\cref{eq:nonhomogeneous:1}?
%
%\end{frame}

\subsection{9.5 Formalizing our approach to recurrence equations}

\begin{frame}[c]
    \frametitle{Formalizing everything}
    
    See \href{https://www.rellek.net/book/s_recurrence_rigorous.html}{Applied
    Combinatorics 9.5} for a justification of what we did.

    Alternatively, for a linear-algebra approach, see pp.\ 474--480 of 
    this \href{https://hefferon.net/linearalgebra/}{free textbook} by Jim Hefferson.
\end{frame}


\section{9.6 Using Generating Functions to Solve Recurrences}

\begin{frame}
    \frametitle{An old problem}
    
    Recall that we have studied how to solve the recursion
    \begin{equation*}
        (A^{2} + A − 6) r = 0,
    \end{equation*}
    with initial conditions $r_{0} = 1$, $r_{1} = 3$.

    This means
    \begin{equation*}
        r_{n+2} + r_{n+1} - 6 r_{n} = 0.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{A new trick}
    
    The GF of $(r_{n})_{n \ge 0}$ is
    \begin{equation*}
        f(x) = r_{0} + r_{1} x^{2} + r_{2} x^{3} + \cdots
    \end{equation*}

    Then
    \begin{equation*}
        f (x) 
        = \frac{1 + 4x}{1 + x − 6x^2}
        \pause{}
        =
        \frac{-1}{\left(5 \left(3 x + 1\right)\right)} 
        +
        \frac{- 6}{\left(5 \left(2 x - 1\right)\right)}
        .
    \end{equation*}
    \pause{}
    Or you can simply use
    \href{https://www.wolframalpha.com/input/?i=SeriesCoefficient\%5B+\%281+\%2B+4x\%29\%2F\%281+\%2B+x+\%E2\%88\%92+6x\%5E2+\%29\%2C+\%7Bx\%2C+0\%2C+n\%7D\%5D}{WolframAlpha},
    we have
    \begin{equation*}
        r_{n} 
        = 
        \frac{1}{5} (3 \times 2^{n + 1} - (-3)^n)
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Non-homogeneous recurrence}

    Assume that $r_{0}=2$, $r_{1}=1$ and for $n \ge 2$
    \begin{equation*}
        r_{n} − r_{n−1} − 2 r_{n−2} = 2^n.
    \end{equation*}
    Then the generating function of $(r_{n})_{n \ge 0}$ is
    \begin{equation*}
        R(x) = \frac{6x^2 − 5x + 2}{(1 − 2x)(1 − x − 2x^2)}
        .
    \end{equation*}
\end{frame}

%\begin{frame}[standout]
%    Let's play a game
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=\linewidth]{./mario.jpg}
%    \end{figure}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Game rules}
%
%    In what follows, are five \alert{hard} problems.
%
%    You will be divided into groups and work on problems of your choice for \emph{30}
%    minutes.
%
%    When the time is up, groups will compete with each other to answer the questions.
%
%    Members of the 🏆️ team gets 0.5 bonus points in their final grades.
%\end{frame}
%
%\begin{frame}
%    \frametitle{How to make a good team?}
%    
%    In your group, you should have
%
%    \begin{itemize}
%        \item A \emph{solver} 🤖️ who leads the effort of solving problems
%        \item A \emph{scribe} 🎤️ who keeps notes of your answers.
%        \item A \emph{reporter} 📺️ who shares the solution with the class.
%    \end{itemize}
%\end{frame}
%
\section{A Few Extra Questions}

\begin{frame}
    \frametitle{Three smart monkeys}
    
    You are visiting a magical zoo where there are three \emoji{monkey-face} A, B, C who can speak
    English.

    You know that a monkey is either honest (always telling truth) or dishonest
    (always telling lies).

    The three monkeys tell you
    \begin{itemize}
        \item A -- B is a liar implies that C is honest.
        \item B -- A is a liar.
        \item C -- A says B is a liar.
    \end{itemize}
    Can you distinguish honest and dishonest \emoji{monkey-face}?
\end{frame}

%\begin{frame}
%    \frametitle{A mysterious identity}
%    
%    For integers \(m \ge 0\) and \(n \ge 0\), we have
%    \[
%        \sum_{0 \le k \le m} \binom{n-k}{m-k}
%        =\binom{n+1}{m}.
%    \]
%
%    Prove this using a \emph{combinatorial} argument.
%\end{frame}

%\begin{frame}
%    \frametitle{A strange chessboard}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.3\linewidth]{./chess-2.png}
%    \end{figure}
%
%    In how many ways can we put four rooks on the chessboard above so that 
%    \begin{enumerate}
%        \item no two rooks are in the same row or column,
%        \item and no rook is in a yellow cell.
%    \end{enumerate}
%
%    Solve this problem with the \emph{inclusion-exclusion principle}.
%\end{frame}

\begin{frame}
    \frametitle{So many circles}
    
    Let $P_{n}$ be the number to stack $n$ circles in a plane such that the bottom
    row consists of $n$ consecutive circles.

    As shown below $P_{3} = 5$.
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{./catalan-coin.png}
    \end{figure}

    Find and prove a closed formula of $P_{n}$.
\end{frame}

%\begin{frame}
%    \frametitle{So many trees}
%    
%    In a binary tree, a node has either zero, or two child nodes.
%
%    The size of a binary is the number of its internal nodes.
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\linewidth]{./bin-tree.png}
%        \caption*{Binary trees of sizes $0, 1, 2, 3$.}%
%    \end{figure}
%
%    Let $B_{n}$ be the number of binary trees of size $n$.
%
%    Find the generating function of $B_{n}$ and get a closed formula for $B_{n}$ from
%    the GF.
%\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-14.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignment problems will \emph{not} be graded but they will appear in quizzes!

            \href{https://www.rellek.net/book/app-comb.html}{Applied Combinatorics}
            \begin{itemize}
                \item[\emoji{pencil}] Section 9.9: 9, 11, 13, 15, 17.
            \end{itemize}

            \hint{} For some problems, you can use WolframAlpha like 
            \href{https://www.wolframalpha.com/input/?i=SeriesCoefficient\%5Bx\%2F\%281-x\%29*\%281\%2Bx\%2Bx\%5E2\%2Bx\%5E3\%29\%281\%2F\%281-x\%5E4\%29\%29\%281\%2F\%281-x\%29\%29\%2C+\%7Bx\%2C+0\%2C+n\%7D\%5D}{this}.
        \end{column}
    \end{columns}
\end{frame}

\end{document}
