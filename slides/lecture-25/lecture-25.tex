\input{../meta.tex}

\title{Lecture 25}

\begin{document}

\maketitle

\section{AC 10 Probability}

\subsection{10.1 An Introduction to Probability}

\begin{frame}[c]
    \frametitle{Professor C's game}

    Professor C told his students --- 

    \vspace{1em}

    ``\emph{If there is a problem in the final exam which you cannot answer,
        you can roll a pair of \emoji{game-die} given to your by the professor instead. 
        If the difference of the outcomes are $d$, you get
    $d-2$ points for the problem.}''

    \vspace{1em}

    Should you play this game or leave the question unanswered?
\end{frame}

\begin{frame}
    \frametitle{Probability Space}
    
    Let $S$ be a finite set and $P$ be a function.
    The pair $(S,P)$ is called a \alert{probability space} if

    \begin{itemize}
        \item $P$'s domain is the family (set) of all subsets of $S$.
        \item $P$'s range is $[0,1]$.
        \item $P(\emptyset) = 0$ and $P(S) = 1$.
        \item If $A, B \subseteq S$ and $A \cap B = \emptyset$, then $P(A \cup B) =
            P(A) + P(B)$.
    \end{itemize}

    \cake{} What is $S$ in Professor C's game?
\end{frame}

\begin{frame}
    \frametitle{What is probability?}
    
    An element $x$ of $S$ is called an \alert{outcome}. 
    The quantity $P(x) = P(\{x\})$ is the \alert{probability} of $x$.

    A subset $E$ of $S$ is called an \alert{event}. 
    The quantity
    \begin{equation*}
    P(E) = \sum_{x \in E} P(x)
    \end{equation*}
    is the \alert{probability} of $E$.

    \cake{} What is $P$ in Professor C's game?
\end{frame}

\begin{frame}[c]
    \frametitle{Example 10.2}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            If we give the spinner a push, it will stop at a uniform random position.

            \cake{} What is $S$ and what is $P$?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{spinner.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 10.6}
    
    A \emoji{backpack} contains twenty \emoji{teddy-bear},
    of which six are {\color{red}red},
    nine are {\color{blue}blue} and the remaining five are {\color{green}green}. 

    Three of the twenty \emoji{teddy-bear} are selected uniformly at random.

    Let $S = \{0, 1, 2, 3, 4, 5\}$.

    For $x\in S$, let $P(x)$ be the probability of having $x$ {\color{blue} blue}
    \emoji{teddy-bear} in the three selected ones.

    \think{} Can we compute $P(x)$?
\end{frame}

\subsection{10.2 Conditional Probability and Independent Events}

\begin{frame}
    \frametitle{Two \emoji{teddy-bear}}
        A \emoji{backpack} contains twenty \emoji{teddy-bear},
        of which six are {\color{red}red},
        nine are {\color{blue}blue} and the remaining five are {\color{green}green}. 

        While blindfolded, Professor C selects two of the twenty \emoji{teddy-bear} random  
        and puts one in his left pocket and one in his right pocket. 

        \pause{}

        \cake{} 
        What is the probability that the \emoji{teddy-bear} in his left pocket is
        {\color{red}red}?

        \pause{}

        Now Professor C that reveals \emoji{teddy-bear} in his right pocket is {\color{blue}blue}. 
        The probability that the \emoji{teddy-bear} in his left pocket is {\color{red}red} becomes higher.
        But how much?
\end{frame}

\begin{frame}
    \frametitle{Conditional probability}
    
    Let $B$ be an event with $P(B) > 0$. The for every event $A$, the \alert{probability of
    $A$ given $B$} is
    \begin{equation*}
        P(A \mid B) = \frac{P(A \cap B)}{P(B)}
    \end{equation*}

    \begin{exampleblock}{Two \emoji{teddy-bear}}
    Let 
    \begin{itemize}
        \item $A$ be the ``left \emoji{teddy-bear} is {\color{red}red}'' 
        \item $B$ be the ``left \emoji{teddy-bear} is {\color{blue}blue}'' 
    \end{itemize}
    Then
    \begin{equation*}
        P(A \mid B) 
        = 
        \frac{P(A \cap B)}{P(B)}
        =
        \txtq{}
    \end{equation*}
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Example 10.9}
    
    We are given a second \emoji{backpack} containing twenty \emoji{teddy-bear},
    of which ten are {\color{red}red},
    five are {\color{blue}blue} and the remaining three are {\color{green}green}. 

    One of the \emoji{backpack} is selected at random, and two
    \emoji{teddy-bear} are chosen at random.

    \cake{} What is the probability that both of them are green?
\end{frame}

\begin{frame}
    \frametitle{Example 10.10}
    
    Two events $A$ and $B$ are \alert{independent} if $P(A \cap B) = P(A)P(B)$.

    Given the two previous \emoji{backpack}, we chose one at random and then choose a
    \emoji{teddy-bear} from it at random.

    Let $A$ be the event that the second \emoji{backpack} is chosen.

    Let $B$ be the event that the \emoji{teddy-bear} is green.

    \think{} Is $A$ and $B$ independent?
\end{frame}

\begin{frame}[t]
    \frametitle{Example 10.11}
    
    A pair of \emoji{game-die} are rolled, one red and one blue. 

    Let $A$ be the event that the red die shows either a $3$ or a $5$, 
    and let $B$ be the event that you get doubles. 

    \think{} Are $A$ and $B$ independent?
\end{frame}

\subsection{10.3 Bernoulli Trials}

\begin{frame}
    \frametitle{Flip a \emoji{coin}}
    
    Assume flipping \emoji{coin} has probability $p$ to show a head.

    \think{} What is the probability to see $2$ heads in $3$ \emoji{coin} flipping?
\end{frame}

\begin{frame}[c]
    \frametitle{Bernoulli trials}
    
    If we repeat the experiment $n$ times, then probability of succeed $i$ times is
    \begin{equation*}
        P(i) = \binom{n}{i} p^{i} (1-p)^{n-i}.
    \end{equation*}

    \pause{}
    
    A fair \emoji{coin} is tossed $100$ times.

    Then the probability of getting heads $40$ times and tails the other $60$ times is
    \begin{equation*}
        \binom{100}{40}
        \left(\frac{1}{2}\right)^{40}
        \left(\frac{1}{2}\right)^{60}
        =
        \binom{100}{40}
        \left(\frac{1}{2}\right)^{100}
        .
    \end{equation*}
\end{frame}

\subsection{10.4 Discrete Random Variables}

\begin{frame}[c]
    \frametitle{Random Variables}
    
    Let $(S, P)$ be a probability space 
    and let $X : S \mapsto \dsR$ be a function.
    We call $X$ a \alert{random variable}.

    The quantity 
    \begin{equation*}
        E(X) = \sum_{x \in S} X(x)P(x),
    \end{equation*}
    is the \alert{expectation} of $X$.
\end{frame}

\begin{frame}
    \frametitle{A fair \emoji{game-die}}

    The outcomes of rolling a fair die is $S = \{1,2,3,4,5,6\}$.

    Let $X(i) = i$ and $Y(i) = i^{2}$. 

    \think{} Then what are $E(X)$ and $E(Y)$?
\end{frame}

\begin{frame}[t]
    \frametitle{Professor C's game}
    
    In Professor C's game, the outcomes are $S = \{0, 1, 2, 3, 4, 5\}$.

    Let $X(d) = d-2$ be the grade you get for outcome $d$.

    \think{} What is $E(X)$?
\end{frame}

\begin{frame}
    \frametitle{Linearity of expectation}

    \begin{exampleblock}{Proposition 10.17 (AC)}
    Let $X_{1}, X_{2}, \ldots X_{n}$ be random variables. Then
    \begin{equation*}
        E(X_{1} + X_{2} + \cdots + X_{n})
        =
        E(X_{1}) + E(X_{2}) + \cdots + E(X_{n})
        .
    \end{equation*}
    \end{exampleblock}

\end{frame}

\begin{frame}
    \frametitle{Expectation of Bernoulli Trials}
    
    Let $X$ be the number success in $n$ Bernoulli trials, each with probability $p$
    to succeed.
    Let $X_{i}=1$ if the $i$-th experiment succeeds and $X_{i}=0$ otherwise.
    Then 
    \begin{equation*}
        E(X) 
        =
        E\left(\sum_{i=1}^{n} X_{i}\right)
        = 
        \sum_{i=1}^{n} E(X_{i})
        =
        \question{}
    \end{equation*}
\end{frame}

\begin{frame}[c]
    \frametitle{The one thing that you should remember from today's course}
    
    Supposed that you are trying to decide where to do a PhD.

    You can make a table to compute the \alert{expected return} of each one,
    and choose the one scores highest.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{university.png}
        \caption{Expected return in attending universities}%
    \end{figure}
\end{frame}

%\section{Central Tendency}
%
%\begin{frame}[t]
%    \frametitle{Markov's inequality}
%
%    \begin{block}{Theorem 10.20 (AC)}
%        Let $X$ be a random variable. Then for any $k > 0$,
%        \begin{equation*}
%            P(\abs{X} \ge k) \le \frac{E(\abs{X})}{k}.
%        \end{equation*}
%    \end{block}
%\end{frame}
%
%\begin{frame}
%    \frametitle{The variance}
%    
%    The quantity $\Var(X) = E((X-E(X)^{2})$ is called the \alert{variance} of
%    $X$.
%
%    \begin{exampleblock}{Bernoulli trials}
%        In a Bernoulli trails with $n$ experiments, the number of success $X$ has
%        $E(X) = n p$.
%        \begin{equation*}
%        \Var(X) = 
%        \sum_{i=0}^{n}
%        (i- n p)^{2}
%        \binom{n}{i} p^{i} (1-p)^{n-i}
%        =
%        n p (1-p)
%        .
%        \end{equation*}
%    \end{exampleblock}
%
%    We can get this by using
%    \begin{equation*}
%        \Var(X_{1}+X_{2}+\cdots+X_{n})
%        =
%        \Var(X_{1})+\Var(X_{2})+\cdots+\Var(X_{n})
%        ,
%    \end{equation*}
%    when $X_{1}, \ldots, X_{n}$ are 
%    \href{https://mathworld.wolfram.com/IndependentVariable.html}{independent}.
%\end{frame}
%
%\begin{frame}[t]
%    \frametitle{Chebyshev's Inequality}
%    
%    \begin{block}{Theorem 10.24 (AC)}
%        Let $X$ be a random variable. Then for all $k>0$,
%        \begin{equation*}
%            P\left(\abs{X - E(X)} > k \sqrt{\Var(X)}\right) \le \frac{1}{k^{2}}.
%        \end{equation*}
%    \end{block}
%
%    \think{} Can you get an upper bound of the probability of getting at most $2000$ or
%    at least $8000$ heads in $10000$ tosses of a fair coin?
%\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-25.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignment problems will \emph{not} be graded but they will appear in quizzes!

            \href{https://www.rellek.net/book/app-comb.html}{Applied Combinatorics}
            \begin{itemize}
                \item[\emoji{pencil}] Section 10.8: 1, 2, 3, 4, 5, 6.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
